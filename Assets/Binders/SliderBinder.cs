// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (Slider))]
    [AddComponentMenu("UI/Bindings/SliderBinder")]
    public class SliderBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding EnabledBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(float))]
        public Binding MaxValueBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(float))]
        public Binding MinValueBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(float))]
        public Binding ValueBinding = new Binding();

        private Slider _slider;

		protected override void OnAwake()
        {
			_slider = GetComponent<Slider>();

			_slider.onValueChanged.AddListener(Slider_OnValueChangedHandler);

			MinValueBinding.Changed += MinValueBinding_ChangedHandler;
			MaxValueBinding.Changed += MaxValueBinding_ChangedHandler;
			ValueBinding.Changed += ValueBinding_ChangedHandler;
			EnabledBinding.Changed += EnabledBinding_ChangedHandler;
        }

        private void Slider_OnValueChangedHandler(float obj)
        {
            SetValue(ValueBinding.Path, obj);
        }

        private void EnabledBinding_ChangedHandler(object obj)
        {
            _slider.interactable = (bool)obj;
        }
        private void ValueBinding_ChangedHandler(object obj)
        {
            _slider.value = (float)obj;
        }
        private void MinValueBinding_ChangedHandler(object obj)
        {
            _slider.minValue = (float)obj;
        }
        private void MaxValueBinding_ChangedHandler(object obj)
        {
            _slider.maxValue = (float)obj;
        }
    }
}