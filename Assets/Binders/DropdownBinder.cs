// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Observable;

namespace Databinding.Binders
{
    [RequireComponent(typeof(Dropdown))]
    [AddComponentMenu("UI/Bindings/DropdownBinder")]
    public class DropdownBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(IEnumerable))]
        public Binding ItemsBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding EnabledBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties)]
        public Binding SelectedBinding = new Binding();

        private IReadOnlyObservableCollection _items;
        private Dropdown _dropdown;

		protected override void OnAwake()
        {
			_dropdown = GetComponent<Dropdown>();
			_dropdown.onValueChanged.AddListener(Dropdown_ValueChangedHandler);

            ItemsBinding.Changed += ItemsBinding_ChangedHandler;
            EnabledBinding.Changed += EnabledBinding_ChangedHandler;
            SelectedBinding.Changed += SelectedBinding_ChangedHandler;
        }

        protected void ItemsBinding_ChangedHandler(object obj)
        {
            if (_items == obj)
                return;

            if (_items != null)
                _items.CollectionChanged -= Items_CollectionChanged;

            OnClear();

            _items = obj as IReadOnlyObservableCollection;

            if (_items != null)
                _items.CollectionChanged += Items_CollectionChanged;

            int index = 0;
            foreach (var item in _items)
                OnAdd(index++, item);
        }

        private void Items_CollectionChanged(object sender, CollectionChangedEventArgs e)
        {
            int index = 0;
            switch (e.Action)
            {
                case CollectionChangedActions.Insert:
                    {
                        index = e.NewStartingIndex;
                        foreach (var item in e.NewItems)
                            OnAdd(index++, item);
                        break;
                    }
                case CollectionChangedActions.Remove:
                    {
                        foreach (var item in e.OldItems)
                            OnRemove(item);
                        break;
                    }
                case CollectionChangedActions.Replace:
                    index = e.NewStartingIndex;
                    foreach (var item in e.NewItems)
                        OnReplace(index++, item);
                    break;
                case CollectionChangedActions.Move:
                    {
                        int oldIndex = e.OldStartingIndex;
                        int newIndex = e.NewStartingIndex;
                        foreach (var item in e.NewItems)
                            OnMove(oldIndex++, newIndex++, item);
                        break;
                    }
                case CollectionChangedActions.Clear:
                    {
                        OnClear();
                        break;
                    }
            }
        }

        private void Dropdown_ValueChangedHandler(int value)
        {
            if (false == string.IsNullOrEmpty(SelectedBinding.Path))
            {
                if (value == -1)
                    SetValue(SelectedBinding.Path, null);
                else
                {
                    var selectedItem = _items[value];
                    SetValue(SelectedBinding.Path, selectedItem);
                }
            }
        }

        private void EnabledBinding_ChangedHandler(object obj)
        {
            _dropdown.interactable = (bool)obj;
        }

        private void SelectedBinding_ChangedHandler(object obj)
        {
            var index = _items.IndexOf(obj);
            _dropdown.value = Mathf.Clamp(index, 0, int.MaxValue);
        }

        private void OnAdd(int index, object obj)
        {
            var option = obj as IOptionData;

            if (option != null)
                _dropdown.options.Add(new Dropdown.OptionData(option.OptionLabel, option.OptionImage));
            else
                _dropdown.options.Add(new Dropdown.OptionData(obj.ToString()));
        }

        private void OnRemove(object obj)
        {
            var index = _items.IndexOf(obj);
            Assert.IsFalse(index < 0);
            _dropdown.options.RemoveAt(index);
        }

        private void OnReplace(int index, object obj)
        {
            _dropdown.options.RemoveAt(index);
            OnAdd(index, obj);
        }

        private void OnMove(int oldIndex, int newIndex, object obj)
        {
            if (oldIndex == newIndex)
                return;

            var index = _items.IndexOf(obj);
            var option = _dropdown.options[oldIndex];

            _dropdown.options.RemoveAt(index);
            _dropdown.options.Insert(newIndex, option);
        }

        private void OnClear()
        {
            _dropdown.options.Clear();
        }

        public interface IOptionData
        {
            string OptionLabel { get; set; }
            Sprite OptionImage { get; set; }
        }
    }
}