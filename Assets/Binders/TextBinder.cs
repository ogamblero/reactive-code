﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof(Text))]
    [AddComponentMenu("UI/Bindings/TextBinder")]
    public class TextBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties)]
        public Binding TextBinding = new Binding();

        private Text _text;

        protected override void OnAwake()
        {
            _text = GetComponent<Text>();
            TextBinding.Changed += TextBinding_ChangedHandler;
        }

        private void TextBinding_ChangedHandler(object obj)
        {
            _text.text = obj != null ? obj.ToString() : string.Empty;
        }
    }
}