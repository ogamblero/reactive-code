// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System.Collections;
using System.Collections.Generic;
using System.Collections.Observable;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Databinding.Binders
{
    [AddComponentMenu("UI/Bindings/ListBinder")]
    public class ListBinder : Binder
    {
        public BindingContext ItemPrefab;

        [BindingConstrains(BindingTypes.Properties, typeof(IEnumerable))]
        public Binding ItemsBinding = new Binding();

        private IReadOnlyObservableCollection _items;

        private readonly List<BindingContext> _itemsView = new List<BindingContext>();
        private RectTransform _viewRect;
        private RectTransform _itemViewRect;

		protected override void OnAwake()
        {
            _viewRect = GetComponent<RectTransform>();
            _itemViewRect = ItemPrefab.GetComponent<RectTransform>();

			ItemsBinding.Changed += ItemsBinding_ChangedHandler;

            Assert.IsNotNull(ItemPrefab);

            var scale = ItemPrefab.transform.localScale;
            ItemPrefab.transform.SetParent(transform.parent);
            ItemPrefab.transform.localScale = scale;
            ItemPrefab.gameObject.SetActive(false);
        }

        protected void ItemsBinding_ChangedHandler(object obj)
        {
            if (_items == obj)
                return;

            if (_items != null)
                _items.CollectionChanged -= Items_CollectionChanged;

            OnClear();

            _items = obj as IReadOnlyObservableCollection;

            if (_items != null)
                _items.CollectionChanged += Items_CollectionChanged;

            int index = 0;
            foreach (var item in _items)
                OnAdd(index++, item);
        }

        private void Items_CollectionChanged(object sender, CollectionChangedEventArgs e)
        {
            int index = 0;
            switch (e.Action)
            {
                case CollectionChangedActions.Insert:
                    {
                        index = e.NewStartingIndex;
                        foreach (var item in e.NewItems)
                            OnAdd(index++, item);
                        break;
                    }
                case CollectionChangedActions.Remove:
                    {
                        foreach (var item in e.OldItems)
                            OnRemove(item);
                        break;
                    }
                case CollectionChangedActions.Replace:
                    index = e.NewStartingIndex;
                    foreach (var item in e.NewItems)
                        OnReplace(index++, item);
                    break;
                case CollectionChangedActions.Move:
                    {
                        int oldIndex = e.OldStartingIndex;
                        int newIndex = e.NewStartingIndex;
                        foreach (var item in e.NewItems)
                            OnMove(oldIndex++, newIndex++, item);
                        break;
                    }
                case CollectionChangedActions.Clear:
                    {
                        OnClear();
                        break;
                    }
            }
        }

        private void OnRemove(object obj)
        {
            var itemView = _itemsView.FirstOrDefault(o => o.Source == obj);

            Recycle(itemView.gameObject);
            _itemsView.Remove(itemView);
        }

        private void OnReplace(int index, object obj)
        {
            var itemView = _itemsView[index];
            itemView.Source = obj;
        }

        private void OnAdd(int index, object obj)
        {
            var itemView = CreateItemView();
            itemView.transform.SetSiblingIndex(index);

            itemView.Source = obj;
            itemView.name = "_Item " + _itemsView.Count;

            _itemsView.Insert(index, itemView);
        }

        private void OnMove(int oldIndex, int newIndex, object obj)
        {
            if (oldIndex == newIndex)
                return;

            var item = _itemsView[oldIndex];
            _itemsView.RemoveAt(oldIndex);
            _itemsView.Insert(newIndex, item);

            item.transform.SetSiblingIndex(newIndex);
        }

        private void OnClear()
        {
            foreach (var item in _itemsView)
                Recycle(item.gameObject);

            _itemsView.Clear();
        }

        private BindingContext CreateItemView()
        {
            GameObject go = Instantiate(ItemPrefab.gameObject, _viewRect.position, _viewRect.rotation) as GameObject;
            go.transform.SetParent(_viewRect);

            go.transform.localScale = _itemViewRect.localScale;
            go.SetActive(true);

            return go.GetComponent<BindingContext>();
        }

        private void Recycle(GameObject go)
        {
            Destroy(go);
        }
    }
}