// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

namespace Databinding.Binders
{
    [AddComponentMenu("UI/Bindings/ActivationBinder")]
    public class ActivationBinder : Binder
    {
        [SerializeField] GameObject[] _targets;
        [SerializeField] GameObject[] _inverseTargets;

        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding ValueBinding = new Binding();

        protected override void OnAwake()
        {
            ValueBinding.Changed += ValueBinding_ChangedHandler;
        }

        public void ValueBinding_ChangedHandler(object obj)
        {
            var b = obj != null && (bool)obj;

            for (int i = 0; i < _targets.Length; i++)
                _targets[i].SetActive(b);
            for (int i = 0; i < _inverseTargets.Length; i++)
                _targets[i].SetActive(false == b);
        }
    }
}