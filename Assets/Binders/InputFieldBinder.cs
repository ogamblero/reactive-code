// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (InputField))]
    [AddComponentMenu("UI/Bindings/InputFieldBinder")]
    public class InputFieldBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding EnabledBinding = new Binding();

        [BindingConstrains(BindingTypes.Commands)]
        public Binding SubmitBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(string))]
        public Binding TextBinding = new Binding();

        private InputField _iField;

        protected override void OnAwake()
        {
			_iField = GetComponent<InputField>();

			_iField.onEndEdit.AddListener(InputField_EndEditHandler);
            _iField.onValueChanged.AddListener(InputField_ValueChangedHandler);

            EnabledBinding.Changed += EnabledBinding_ChangedHandler;
            TextBinding.Changed += TextBinding_ChangedHandler;
        }

        private void InputField_ValueChangedHandler(string text)
        {
            SetValue(TextBinding.Path, text);
        }
        private void InputField_EndEditHandler(string text)
        {
            SetValue(SubmitBinding.Path, text);
        }

        private void EnabledBinding_ChangedHandler(object obj)
        {
            _iField.interactable = (bool)obj;
        }
        private void TextBinding_ChangedHandler(object obj)
        {
            _iField.text = obj != null ? obj.ToString() : string.Empty;
        }
    }
}