// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (Image))]
    [AddComponentMenu("UI/Bindings/ImageBinder")]
    public class ImageBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(Color))]
        public Binding ColorBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(Texture2D), typeof(Sprite))]
        public Binding SpriteBinding = new Binding();

        private Image _image;
        private Sprite _initSprite;

        protected override void OnAwake()
        {
			_image = GetComponent<Image>();
			_initSprite = _image.sprite;

			SpriteBinding.Changed += SpriteBinding_ChangedHandler;
			ColorBinding.Changed += ColorBinding_ChangedHandler;
        }

        private void SpriteBinding_ChangedHandler(object obj)
        {
            if (obj is Texture2D)
            {
                var texture = (Texture2D)obj;
                _image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            }
            else if (obj is Sprite)
            {
                _image.sprite = (Sprite)obj;
            }
            else
            {
                _image.sprite = _initSprite;
            }
        }

        private void ColorBinding_ChangedHandler(object arg)
        {
            _image.color = (Color) arg;
        }
    }
}