// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (Image))]
    [AddComponentMenu("UI/Bindings/FillAmmountBinder")]
    public class FillAmmountBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(float))]
        public Binding FillValueBinding = new Binding();

        private Image _image;

        protected override void OnAwake()
        {
			_image = GetComponent<Image>();
			FillValueBinding.Changed += FillValueBinding_ChangedHandler;
        }

        private void FillValueBinding_ChangedHandler(object obj)
        {
            _image.fillAmount = (float)obj;
        }
    }
}