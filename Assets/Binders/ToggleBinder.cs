// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (Toggle))]
    [AddComponentMenu("UI/Bindings/ToggleBinder")]
    public class ToggleBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding EnabledBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding ValueBinding = new Binding();
				
        private Toggle _toggle;
        private BindingParamater _paramater;

        protected override void OnAwake()
        {
            _toggle = GetComponent<Toggle>();
            _paramater = GetComponent<BindingParamater>();

            _toggle.onValueChanged.AddListener(Toggle_ValueChangedHandler);

			ValueBinding.Changed += ValueBinding_ChangedHandler;
			EnabledBinding.Changed += EnabledBinding_ChangedHandler;
        }

        private void Toggle_ValueChangedHandler(bool value)
        {
            object arg = _paramater == null ? null : _paramater.GetValue();
            SetValue(ValueBinding.Path, arg);
        }

        private void EnabledBinding_ChangedHandler(object obj)
        {
            _toggle.interactable = (bool)obj;
        }
        private void ValueBinding_ChangedHandler(object obj)
        {
            _toggle.isOn = (bool)obj;
        }
    }
}