// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

namespace Databinding.Binders
{
    [AddComponentMenu("UI/Bindings/KeyBinder")]
    public class KeyBinder : Binder
    {
        public KeyCode Key;

        [BindingConstrains(BindingTypes.Commands)]
        public Binding CommandBinding = new Binding();

        private void Update()
        {
            if (Input.GetKeyUp(Key))
                SetValue(CommandBinding.Path, null);
        }
    }
}