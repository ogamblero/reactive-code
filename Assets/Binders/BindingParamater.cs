// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

namespace Databinding.Binders
{
    [AddComponentMenu("UI/Bindings/BindingParamater")]
    public class BindingParamater : Binder
    {
        public ParamaterTypes ParamaterType;
        public string StaticParameter;

        [BindingConstrains(BindingTypes.Properties)]
        public Binding ParameterBinding = new Binding();

        public object GetValue()
        {
            switch (ParamaterType)
            {
                case ParamaterTypes.Binding:
                    return GetValue(ParameterBinding.Path);
                case ParamaterTypes.Context:
                    return Context.Source;
                case ParamaterTypes.Static:
                    return StaticParameter;
            }

            return null;
        }

        public enum ParamaterTypes
        {
            Context,
            Static,
            Binding
        }
    }
}