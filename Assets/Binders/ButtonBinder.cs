// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (Button))]
    [AddComponentMenu("UI/Bindings/ButtonBinder")]
    public class ButtonBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(bool))]
        public Binding EnabledBinding = new Binding();

        [BindingConstrains(BindingTypes.Commands)]
        public Binding OnClickBinding = new Binding();

        private Button _button;
        private BindingParamater _paramater;

		protected override void OnAwake()
        {
			_button = GetComponent<Button>();
            _paramater = GetComponent<BindingParamater>();

            _button.onClick.AddListener(Call);
			EnabledBinding.Changed += EnabledBinding_ChangedHandler;
        }

        public void Call()
        {
            object arg = _paramater == null ? null : _paramater.GetValue();
            SetValue(OnClickBinding.Path, arg);
        }

        private void EnabledBinding_ChangedHandler(object obj)
        {
            _button.interactable = (bool)obj;
        }
    }
}