// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using UnityEngine.UI;

namespace Databinding.Binders
{
    [RequireComponent(typeof (RawImage))]
    [AddComponentMenu("UI/Bindings/RawImageBinder")]
    public class RawImageBinder : Binder
    {
        [BindingConstrains(BindingTypes.Properties, typeof(Color))]
        public Binding ColorBinding = new Binding();

        [BindingConstrains(BindingTypes.Properties, typeof(Texture2D))]
        public Binding SpriteBinding = new Binding();

        private RawImage _image;

        protected override void OnAwake()
        {
			_image = GetComponent<RawImage>();

			SpriteBinding.Changed += SpriteBinding_ChangedHandler;
			ColorBinding.Changed += ColorBinding_ChangedHandler;
        }

        private void SpriteBinding_ChangedHandler(object obj)
        {
            _image.texture = obj as Texture2D;
        }
        private void ColorBinding_ChangedHandler(object obj)
        {
            _image.color = (Color)obj;
        }
    }
}