﻿// Designed by Ivan Dudinov
// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

namespace System.Observable
{
    public interface IReadOnlyProperty
    {
        object RawValue { get; }
        event Action Changed;
    }

    public interface IReadOnlyProperty<T> : IReadOnlyProperty
    {
        T Value { get; }
        event Action<T> ValueChanged;
    }

    public interface IReadOnlyProperty<T, TSender> : IReadOnlyProperty<T>
    {
        event Action<TSender> SenderChanged;
    }

    [Serializable]
    public abstract class Property : IReadOnlyProperty
    {
        public event Action Changed;

        public virtual Type ValueType { get { return typeof(object); } }

        public abstract object RawValue { get; }

        private int _monitor;

        public virtual void Reset()
        {
            Changed = null;
        }

        protected virtual void RaiseChanged()
        {
            CheckReentrancy();

            if (Changed == null)
                return;

            ++_monitor;
            Changed();
            --_monitor;
        }

        private void CheckReentrancy()
        {
            if (_monitor > 0 && Changed != null && Changed.GetInvocationList().Length > 1)
                throw new InvalidOperationException();
        }
    }

    [Serializable]
    public class Property<T> : Property, IReadOnlyProperty<T>
    {
        private static readonly bool IsValueType = typeof(T).IsValueType;

        public event Action<T> ValueChanged;

        public T Value { get; protected set; }
        public override Type ValueType { get { return typeof(T); } }

        public sealed override object RawValue { get { return Value; } }

        private bool _monior;

        public Property()
        { }
        public Property(T initValue)
        {
            Value = initValue;
        }

        public bool Set(T value)
        {
            if (_monior)
                return false;

            _monior = true;

            bool changed = false;
            if (IsValueType)
                changed = false == System.ValueType.Equals(Value, value);
            else
                changed = false == object.Equals(Value, value);

            if (changed)
            {
                Value = value;
                RaiseChanged();
            }

            _monior = false;

            return changed;
        }

        protected override void RaiseChanged()
        {
            base.RaiseChanged();

            if (ValueChanged != null)
                ValueChanged(Value);
        }

        public sealed override void Reset()
        {
            base.Reset();

            ValueChanged = null;
            Value = default(T);
        }

        public override string ToString()
        {
            var valueStr = IsValueType
                ? Value.ToString()
                : (ReferenceEquals(Value, null) ? "NULL" : Value.ToString());

            return '[' + valueStr + ']';
        }
    }

    [Serializable]
    public class Property<T, TSender> : Property<T>, IReadOnlyProperty<T, TSender>
    {
        public event Action<TSender> SenderChanged;

        private readonly TSender _sender;

        public Property(TSender sender)
            : base()
        {
            _sender = sender;
        }

        public Property(TSender sender, T initialValue)
            : base(initialValue)
        {
            _sender = sender;
        }

        protected override void RaiseChanged()
        {
            base.RaiseChanged();

            if (SenderChanged != null)
                SenderChanged(_sender);
        }
    }
}