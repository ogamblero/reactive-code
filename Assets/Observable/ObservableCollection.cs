﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace System.Collections.Observable
{
    public interface IReadOnlyObservableCollection : IEnumerable
    {
        event Action<object, CollectionChangedEventArgs> CollectionChanged;

        object this[int index] { get; }
        int Count { get; }

        int IndexOf(object item);
        bool Contains(object item);
        void CopyTo(object[] array, int arrayIndex);
    }

    public interface IReadOnlyObservableCollection<T> : IEnumerable<T>, IReadOnlyObservableCollection
    {
        new T this[int index] { get; }

        int IndexOf(T item);
        bool Contains(T item);
        void CopyTo(T[] array, int arrayIndex);
    }

    [Serializable]
	public class ObservableCollection<T> : Collection<T>, IReadOnlyObservableCollection<T>
    {
        public virtual event Action<object, CollectionChangedEventArgs> CollectionChanged;

        private int _monitor;

        T IReadOnlyObservableCollection<T>.this[int index] { get { return this[index]; } }

        public ObservableCollection()
        { }
        public ObservableCollection(IList<T> list)
            : base(list != null ? new List<T>(list.Count) : list)
        {
            CopyFrom(list);
        }
        public ObservableCollection(IEnumerable<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            CopyFrom(collection);
        }

        public void Move(int oldIndex, int newIndex)
        {
            MoveItem(oldIndex, newIndex);
        }

        protected override void ClearItems()
        {
            CheckReentrancy();
            base.ClearItems();
            ReiseCollectionChanged(CollectionChangedEventArgs.Clear());
        }

        protected override void RemoveItem(int index)
        {
            CheckReentrancy();
            T item = this[index];
            base.RemoveItem(index);
            ReiseCollectionChanged(CollectionChangedEventArgs.Remove(index, item));
        }

        protected override void InsertItem(int index, T item)
        {
            CheckReentrancy();
            base.InsertItem(index, item);
            ReiseCollectionChanged(CollectionChangedEventArgs.Insert(index, item));
        }

        protected override void SetItem(int index, T item)
        {
            CheckReentrancy();
            T oldItem = this[index];
            base.SetItem(index, item);
            ReiseCollectionChanged(CollectionChangedEventArgs.Replace(index, item, oldItem));
        }

        protected virtual void MoveItem(int oldIndex, int newIndex)
        {
            CheckReentrancy();
            T item = this[oldIndex];
            base.RemoveItem(oldIndex);
            base.InsertItem(newIndex, item);
            ReiseCollectionChanged(CollectionChangedEventArgs.Move(oldIndex, newIndex, item));
        }

        protected virtual void ReiseCollectionChanged(CollectionChangedEventArgs e)
        {
            if (CollectionChanged == null)
                return;

            ++_monitor;
            CollectionChanged(this, e);
            --_monitor;
        }

        protected void CheckReentrancy()
        {
            if (_monitor > 0 && CollectionChanged != null && CollectionChanged.GetInvocationList().Length > 1)
				throw new InvalidOperationException();
        }

        private void CopyFrom(IEnumerable<T> collection)
        {
            if (collection == null || Items == null)
                return;

            foreach (T obj in collection)
                Items.Add(obj);
        }

        object IReadOnlyObservableCollection.this[int index] { get { return this[index]; } }

        int IReadOnlyObservableCollection.IndexOf(object item)
        {
            return IndexOf((T)item);
        }

        bool IReadOnlyObservableCollection.Contains(object item)
        {
            return Contains((T)item);
        }

        void IReadOnlyObservableCollection.CopyTo(object[] array, int arrayIndex)
        {
            CopyTo(array.Cast<T>().ToArray(), arrayIndex);
        }
    }

    [Serializable]
    public class ObservableCollection<T, TInterface> : ObservableCollection<T>, IReadOnlyObservableCollection<TInterface>
       where T : TInterface
    {
        public ObservableCollection()
            : base()
        { }

        public ObservableCollection(IEnumerable<T> items)
            : base(items)
        { }

        TInterface IReadOnlyObservableCollection<TInterface>.this[int index] { get { return base[index]; } }

        int IReadOnlyObservableCollection<TInterface>.IndexOf(TInterface item)
        {
            var state = (T)item;
            if (state == null)
                return -1;

            return Items.IndexOf(state);
        }

        bool IReadOnlyObservableCollection<TInterface>.Contains(TInterface item)
        {
            var state = (T)item;
            if (state == null)
                return false;

            return Items.Contains(state);
        }

        void IReadOnlyObservableCollection<TInterface>.CopyTo(TInterface[] array, int arrayIndex)
        {
            if (array == null)
                return;

            var states = new T[array.Length];

            Items.CopyTo(states, arrayIndex);

            for (int i = 0; i < array.Length; ++i)
                array[i] = states[i];
        }

        IEnumerator<TInterface> IEnumerable<TInterface>.GetEnumerator()
        {
            foreach (var state in Items)
                yield return state;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }

    public class CollectionChangedEventArgs : EventArgs
    {
        public CollectionChangedActions Action { get; private set; }
        public IList NewItems { get; private set; }
        public IList OldItems { get; private set; }
        public int NewStartingIndex { get; private set; }
        public int OldStartingIndex { get; private set; }

        private CollectionChangedEventArgs()
        {
            NewStartingIndex = -1;
            OldStartingIndex = -1;
        }

        private void InitializeInsert(int index, object item)
        {
            Action = CollectionChangedActions.Insert;
            NewItems = (item == null) ? null : ArrayList.ReadOnly(new object[] { item });
            NewStartingIndex = index;
        }
        private void InitializeRemove(int index, object item)
        {
            Action = CollectionChangedActions.Remove;
            OldItems = (item == null) ? null : ArrayList.ReadOnly(new object[] { item });
            OldStartingIndex = index;
        }

        /// 
        /// Factories
        /// 

        public static CollectionChangedEventArgs Clear()
        {
            var result = new CollectionChangedEventArgs();
            result.Action = CollectionChangedActions.Clear;

            return result;
        }

        public static CollectionChangedEventArgs Insert(int index, object item)
        {
            var result = new CollectionChangedEventArgs();
            result.InitializeInsert(index, item);
            return result;
        }

        public static CollectionChangedEventArgs Remove(int index, object item)
        {
            var result = new CollectionChangedEventArgs();
            result.InitializeRemove(index, item);
            return result;
        }

        public static CollectionChangedEventArgs Replace(int index, object newItem, object oldItem)
        {
            var result = new CollectionChangedEventArgs();

            result.InitializeRemove(index, oldItem);
            result.InitializeInsert(index, newItem);
            result.Action = CollectionChangedActions.Replace;

            return result;
        }

        public static CollectionChangedEventArgs Move(int oldIndex, int newIndex, object item)
        {
            var result = new CollectionChangedEventArgs();

            result.InitializeRemove(oldIndex, item);
            result.InitializeInsert(newIndex, item);
            result.Action = CollectionChangedActions.Move;

            return result;
        }
    }

    public enum CollectionChangedActions
    {
        Insert,
        Remove,
        Replace,
        Move,
        Clear,
    }
}
