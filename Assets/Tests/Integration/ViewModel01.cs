﻿using System.Collections.Observable;
using System.Observable;
using UnityEngine;

namespace Databinding.Tests
{
    public class ViewModel01 : MonoBehaviour
    {
        public readonly Property<bool> Visibility = new Property<bool>();
        public readonly Property<string> Text = new Property<string>();

        public readonly ObservableCollection<ItemModel> Items = new ObservableCollection<ItemModel>();

        public Entity001 Entity
        {
            get;
            set;
        }

        private int _count;

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
                Visibility.Set(!Visibility.Value);

            /*
            if (Input.GetKeyUp(KeyCode.Q))
                Text.Set((_count++).ToString());
            */

            if (Input.GetKeyUp(KeyCode.Q))
            {
                var item = new ItemModel();
                item.Index.Set(_count++);
                Items.Add(item);
            }

            if (Input.GetKeyUp(KeyCode.W))
            {
                var item = new ItemModel();
                item.Index.Set(_count++);
                Items.Insert(2, item);
            }

            if (Input.GetKeyUp(KeyCode.E))
            {
                Items.RemoveAt(5);
            }

            if (Input.GetKeyUp(KeyCode.R))
            {
                var item = new ItemModel();
                item.Index.Set(_count++);
                Items[3] = item;
            }

            if (Input.GetKeyUp(KeyCode.T))
            {
                Items.Move(0,1);
            }
        }

        public void Increase()
        {
            Text.Set((_count++).ToString());
        }
    }

    public class ItemModel
    {
        public readonly Property<int> Index = new Property<int>();
    }
}