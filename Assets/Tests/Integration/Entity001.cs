﻿using System.Observable;

public class Entity001
{
    public readonly Property<string> EntityText = new Property<string>();
    public IReadOnlyProperty<string> EntityText2
    {
        get { return EntityText; }
    }

    void Start()
    { }

    void Update()
    { }
}
