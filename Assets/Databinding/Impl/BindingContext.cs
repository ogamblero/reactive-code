// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;
using System;
using System.Linq;

namespace Databinding
{
	[AddComponentMenu("Databinding/BindingContext")]
    public class BindingContext : MonoBehaviour
    {
        public event Action<BindingContext> SourceChanged;

        [SerializeField, HideInInspector] private TypeInfo _sourceInfo;
        [SerializeField, HideInInspector] private MonoBehaviour _viewModel;

        public TypeInfo SourceInfo
        {
            get { return _sourceInfo; }
            set
            {
                if (_sourceInfo == value)
                    return;

                _sourceInfo = value;

                if (_sourceInfo == null)
                    return;

                var type = _sourceInfo.TryMatchType();
                if (Source != null && type != null && Source.GetType() != type)
                    Source = null;
            }
        }

        public object Source
        {
            get
            {
                if (_viewModel != null)
                    return _viewModel;

                return _model;
            }
            set
            {
                _model = value;
                _viewModel = _model as MonoBehaviour;

                if (_model != null)
                    _sourceInfo = new TypeInfo(_model.GetType());

                if (null != SourceChanged)
                    SourceChanged(this);
            }
        }

        public MonoBehaviour ViewModel
        {
            get { return _viewModel; }
            set
            {
                if (value == _viewModel)
                    return;

                _viewModel = value;
                Source = _viewModel;
            }
        }

        private object _model;

        /// 
        /// UNITY
        /// 

        private void Awake()
        {
            if (_viewModel != null)
                Source = _viewModel;
        }

        private void OnDestroy()
        {
            Source = null;
        }

        /// 
        /// TYPES
        /// 

        [Serializable]
        public class TypeInfo
        {
            public string AssemblyName;
            public string Namespace;
            public string Name;

            private Type _type;

            public TypeInfo()
            { }
            public TypeInfo(Type type)
            {
                if (type == null)
                    return;

                Name = type.Name;
                Namespace = type.Namespace;
                AssemblyName = type.Assembly.FullName;
            }

            public Type TryMatchType()
            {
                if (_type != null)
                    return _type;

                var types0 = TypeCache.GetTypes()
                        .Where(t => t.Name == Name)
                        .ToArray();

                if (types0.Length == 1)
                {
                    _type = types0[0];
                }
                else if (types0.Length > 1)
                {
                    var types1 = types0
                        .Where(t => t.Namespace == Namespace)
                        .ToArray();

                    if (types1.Length == 0)
                    {
                        _type = types0[0];
                    }
                    else if (types1.Length == 1)
                    {
                        _type = types1[0];
                    }
                    else if (types1.Length > 1)
                    {
                        var types2 = types1
                            .Where(t => t.Assembly.FullName == AssemblyName)
                            .ToArray();

                        _type = types2.Length == 0 ? types1[0] : types2[0];
                    }
                }

                return _type;
            }

            public override bool Equals(object obj)
            {
                var other = obj as TypeInfo;

                if (other == null)
                    return false;

                return this.TryMatchType() == other.TryMatchType();
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
    }
}