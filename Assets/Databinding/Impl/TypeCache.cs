// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Databinding
{
    public class TypeCache
    {
        private static readonly Dictionary<Type, TypeCache> Cache = new Dictionary<Type, TypeCache>();

        private static Assembly[] _assemblies;
        private static string[] _namespaces;
        private static Type[] _types;

        private MemberInfo[] _members;

        static TypeCache()
        {
            InitAssembliesCache();
        }
        public TypeCache(Type t)
        {
            var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            _members = t.GetMembers(flags).ToArray();
        }

        /// 
        /// TYPE CACHE API
        /// 
        public MemberInfo GetMember(string name)
        {
            return _members.FirstOrDefault(o => o.Name == name);
        }

        public static TypeCache Get<T>()
        {
            return Get(typeof(T));
        }

        public static TypeCache Get(Type type)
        {
            if (false == Cache.ContainsKey(type))
                Cache.Add(type, new TypeCache(type));

            return Cache[type];
        }

        /// 
        /// ASSEMBLIES API
        ///

        public static IEnumerable<Assembly> GetAssemblies()
        {
            return _assemblies;
        }

        public static IEnumerable<string> GetNamespaces()
        {
            return _namespaces;
        }

        public static IEnumerable<Type> GetTypes()
        {
            return _types;
        }

        private static void InitAssembliesCache()
        {
            if (_assemblies != null && _types != null && _namespaces != null)
                return;

            _assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(o => !o.Location.Contains("Editor"))
                .OrderBy(o => o.FullName)
                .ToArray();

            _types = _assemblies
                   .SelectMany(o => o.GetTypes())
                   .Where(o => o.IsPublic)
                   .OrderBy(o => o.Name)
                   .ToArray();

            _namespaces = _types
                   .Select(o => o.Namespace)
                   .OrderBy(o => o)
                   .Distinct()
                   .ToArray();
        }
    }


    public static class TypeExtensions
    {
        public static bool HasAttribute<T>(this MemberInfo m) where T : Attribute
        {
            return Attribute.IsDefined(m, typeof(T));
        }

        public static T GetAttribute<T>(this MemberInfo m) where T : Attribute
        {
            return m.GetCustomAttributes(typeof(T), true).FirstOrDefault() as T;
        }

        public static T GetAttribute<T>(this object m, string memberName) where T : Attribute
        {
            var member = TypeCache.Get<T>().GetMember(memberName);

            if (member == null)
                return null;

            return member.GetAttribute<T>();
        }

        public static Type GetMemberType(this MemberInfo member)
        {
            if (member is MethodInfo)
                return ((MethodInfo)member).ReturnType;

            if (member is PropertyInfo)
                return ((PropertyInfo)member).PropertyType;

            return ((FieldInfo)member).FieldType;
        }

        public static Type GetParamaterType(this MemberInfo member)
        {
            if (member is MethodInfo)
            {
                var p = ((MethodInfo)member).GetParameters().FirstOrDefault();

                if (p == null)
                    return null;

                return p.ParameterType;
            }

            if (member is PropertyInfo)
            {
                return ((PropertyInfo)member).PropertyType;
            }

            if (member is FieldInfo)
            {
                return (member as FieldInfo).FieldType;
            }

            return null;
        }

        public static void SetMemberValue(this MemberInfo member, object instance, object value)
        {
            if (member is MethodInfo)
            {
                var method = ((MethodInfo)member);
                if (method.GetParameters().Any())
                    method.Invoke(instance, new[] { value });
                else
                    method.Invoke(instance, null);
            }
            else if (member is PropertyInfo)
            {
                ((PropertyInfo)member).SetValue(instance, value, null);
            }
            else
            {
                ((FieldInfo)member).SetValue(instance, value);
            }
        }

        public static object GetMemberValue(this MemberInfo member, object instance)
        {
            if (member is MethodInfo)
                return ((MethodInfo)member).Invoke(instance, null);
            if (member is PropertyInfo)
                return ((PropertyInfo)member).GetValue(instance, null);

            return ((FieldInfo)member).GetValue(instance);

        }

        public static object GetMemberValue(this object obj, string propertyName)
        {
            string[] path = propertyName.Split('.');

            object instance = obj;
            MemberInfo member = null;
            for (int i = 0; i < path.Length; i++)
            {
                member = TypeCache.Get(instance.GetType()).GetMember(path[i]);
                if (member == null)
                    return null;
                instance = member.GetMemberValue(instance);
                if (null == instance)
                    return null;
            }

            return instance;
        }

        public static T GetMemberValue<T>(this MemberInfo member, object instance)
        {
            return (T)GetMemberValue(member, instance);
        }

        public static T GetMemberValue<T>(this PropertyInfo property)
        {
            var getMethod = property.GetGetMethod();

            var dMethod = new DynamicMethod("_DynamicMethod_", typeof(T), Type.EmptyTypes);

            var ilGen = dMethod.GetILGenerator();
            ilGen.Emit(OpCodes.Ldnull);
            ilGen.Emit(OpCodes.Call, getMethod);
            ilGen.Emit(OpCodes.Ret);

            var _DynamicMethod_ = (Func<T>)dMethod.CreateDelegate(typeof(Func<T>));
            var result = _DynamicMethod_();
            return result;
        }

        public static MemberInfo GetRuntimeMember(this Type t, string name)
        {
            return TypeCache.Get(t).GetMember(name);
        }
    }
}