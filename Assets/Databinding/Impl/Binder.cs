// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using UnityEngine;

using System;
using System.Linq;
using System.Observable;
using System.Reflection;
using System.Collections.Generic;

namespace Databinding
{
    public abstract class Binder : MonoBehaviour
    {
        [SerializeField, HideInInspector] GameObject _proxy;
        [SerializeField, HideInInspector] ContextTypes _contextType;

        public GameObject Proxy { get { return _proxy; } }
        public ContextTypes ContextType { get { return _contextType; } }

        public BindingContext Context
        {
            get { return _context; }
            set
            {
                if (_context == value)
                    return;

                if (_context != null)
					_context.SourceChanged -= ContextSource_ChangedHandler;

                _context = value;

                if (_context != null)
                    _context.SourceChanged += ContextSource_ChangedHandler;

                ContextSource_ChangedHandler(_context);
            }
        }

		private object ContextSource
        {
            get { return _source; }
            set
            {
                if (_source == value)
                    return;

                _source = value;

                foreach (var binding in GetBindings())
                {
                    if (string.IsNullOrEmpty(binding.Path))
                        continue;

                    if (_source != null)
                        binding.Source = _source.GetMemberValue(binding.Path);
                    else
                        binding.Source = null;
                }
            }
        }

        private BindingContext _context;
        private object _source;

        private Binding[] _bindingCache;


        /// 
        /// API
        /// 
        protected object GetValue(string memberName)
        {
            if (null == ContextSource)
                return null;

            var result = ContextSource.GetMemberValue(memberName);

            if (result is Property)
                return (result as Property).RawValue;

            return result;
        }

        protected void SetValue(string memberName, object arg)
        {
            if (string.IsNullOrEmpty(memberName) || null == ContextSource)
                return;

            Type type = ContextSource.GetType();
            var member = type.GetRuntimeMember(memberName);
            if (member == null)
            {
                Debug.LogError("Member not found ! " + memberName + " " + type);
                return;
            }

            if (typeof(Property).IsAssignableFrom(member.DeclaringType))
            {
                var prop = ContextSource.GetMemberValue(memberName) as Property;

                MethodInfo setMethod = prop.GetType().GetMethod("Set", BindingFlags.Instance | BindingFlags.Public);
                setMethod.Invoke(prop, new object[1] { arg });
                return;
            }
            else if (member is MethodInfo)
            {
                var method = member as MethodInfo;
                var converted = Convert(method.GetParamaterType(), arg);
                member.SetMemberValue(ContextSource, converted);
            }
            else
            {
                member.SetMemberValue(ContextSource, arg);
            }
        }

        protected virtual void OnAwake()
        {
        }

        /// 
        /// UNITY
        /// 

        private void Awake()
        {
            Context = null == Proxy 
                ? gameObject.GetComponentInParent<BindingContext>() 
                : Proxy.GetComponentInParent<BindingContext>();

            OnAwake();

            foreach (var binding in GetBindings())
            {
                binding.SetActive(true);
                binding.RaiseCanged();
            }
        }
        private void OnEnable()
        {
            foreach (var binding in GetBindings())
            {
                binding.SetActive(true);
                binding.RaiseCanged();
            }
        }
        private void OnDisable()
        {
            foreach (var binding in GetBindings())
                binding.SetActive(false);
        }
        private void OnDestroy()
        {
            Context = null;
        }

        /// 
        /// HANDLERS
        /// 

        private void ContextSource_ChangedHandler(BindingContext context)
        {
            ContextSource = context != null ? context.Source : null;
        }

        /// 
        /// UTILS
        /// 

        private IEnumerable<Binding> GetBindings()
        {
            if (_bindingCache == null)
            {
                _bindingCache = GetType().GetFields()
                        .Where(o => o.FieldType == typeof(Binding))
                        .Select(o => o.GetValue(this))
                        .Cast<Binding>()
                        .ToArray();
            }

            return _bindingCache;
        }

        private static object Convert(Type type, object value)
        {
            if (value == null)
                return null;

            if (type == null || type.IsInstanceOfType(value))
                return value;

            if (type.IsEnum)
                return Enum.Parse(type, value.ToString());
            else if (type == typeof(string))
                return value.ToString();
            else if (type == typeof(bool))
                return bool.Parse(value.ToString());
            else if (type == typeof(int))
                return int.Parse(value.ToString());
            else if (type == typeof(float))
                return float.Parse(value.ToString());
            else if (type == typeof(long))
                return long.Parse(value.ToString());
            else if (type == typeof(double))
                return double.Parse(value.ToString());
            else if (type == typeof(short))
                return short.Parse(value.ToString());

            return null;
        }

        /// 
        /// TYPES
        /// 
        public class BindingConstrainsAttribute : Attribute
        {
            public BindingTypes BindingType { get; private set; }
            public Type[] FilterTypes { get; private set; }

            public BindingConstrainsAttribute(BindingTypes type, params Type[] filter)
            {
                BindingType = type;
                FilterTypes = filter;
            }
        }

        public enum BindingTypes
        {
            Commands = 1,
            Properties = 2
        }

        public enum ContextTypes
        {
            Native = 0,
            Proxy
        }
    }
}