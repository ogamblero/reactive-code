﻿// Copyright (c) Oles Oleschuk. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Observable;

namespace Databinding
{
    [Serializable]
    public class Binding
    {
		public event Action<object> Changed;

        public string Path;

        public object Source
        {
            get { return _source; }
            set
            {
                _source = value;
                SourceProperty = _source as Property;
                Source_ChangedHandler();
            }
        }

        private Property SourceProperty
        {
			get { return _property; }
            set
            {
                if (value == _property)
                    return;

                if (null != _property)
                    _property.Changed -= Source_ChangedHandler;

                _property = value;

                if (null != _property)
                    _property.Changed += Source_ChangedHandler;
            }
        }

        private object _source;
        private Property _property;
        private bool _active = true;

        /// 
        /// API
        /// 

        public void RaiseCanged()
		{
            Source_ChangedHandler();
        }
        public void SetActive(bool active)
        {
            _active = active;
        }

        /// 
        /// HANDLERS
        /// 

        private void Source_ChangedHandler()
        {
            if (false == _active)
                return;

            if (null != Changed && null != Source)
                Changed(GetValue());
        }

        /// 
        /// INTERNAL
        /// 

        private object GetValue()
        {
            if (_property != null)
                return _property.RawValue;

            return Source;
        }
    }
}