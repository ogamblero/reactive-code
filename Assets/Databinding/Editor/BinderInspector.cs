﻿using UnityEngine;
using UnityEditor;

using Databinding;

namespace DatabindingEditor
{
    [CustomEditor(typeof(Binder), true)]
    public class BinderInspector : Editor
    {
        protected Binder _binder;

        private SerializedProperty _proxyProp;
        private SerializedProperty _contextTypeProp;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();

            _binder = target as Binder;

            _contextTypeProp = serializedObject.FindProperty("_contextType");
            _proxyProp = serializedObject.FindProperty("_proxy");

            EditorGUILayout.PropertyField(_contextTypeProp, new GUIContent("Context"));

            if (_binder.ContextType == Binder.ContextTypes.Proxy)
                EditorGUILayout.PropertyField(_proxyProp, new GUIContent("Proxy"));            

            serializedObject.ApplyModifiedProperties();
        }
    }
}