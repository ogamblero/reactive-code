﻿using UnityEngine;
using UnityEditor;
using System;

using Databinding;

namespace DatabindingEditor
{
    public class TypeSelector : EditorWindow
    {
        private Action<Type> _callback;
        private string _filter;
        private string _initialFilter;

        private Vector2 _scroll;

        public static void Show(string filter, Action<Type> callback)
        {
            var window = (TypeSelector)GetWindow(typeof(TypeSelector));
            window.titleContent.text = "TypeSelector";
            window._filter = filter;
            window._initialFilter = filter;
            window._callback = callback;

            window.Show();
        }

        private void OnGUI()
        {
            DrawEditor();
        }

        private void DrawEditor()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal("box");
                {
                    _filter = EditorGUILayout.TextField("Filter", _filter);
                    if (GUILayout.Button("X", GUILayout.Width(24)))
                        _filter = String.Empty;
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                _scroll = EditorGUILayout.BeginScrollView(_scroll, "box");
                {
                    var types = TypeCache.GetTypes();

                    foreach (var type in types)
                    {
                        bool initial = _initialFilter == type.FullName;
                        if (false == initial && false == type.FullName.Contains(_filter))
                            continue;

                        if (initial)
                            EditorGUILayout.BeginHorizontal("box");
                        else
                            EditorGUILayout.BeginHorizontal();
                        {
                            if (GUILayout.Button("✓", GUILayout.Width(24)))
                            {
                                if (_callback != null)
                                {
                                    _callback(type);
                                    _callback = null;
                                }
                                Close();
                            }
                            EditorGUILayout.LabelField(type.FullName);
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndScrollView();

            }
            EditorGUILayout.EndVertical();
        }
    }
}