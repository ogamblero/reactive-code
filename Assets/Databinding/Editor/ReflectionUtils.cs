using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Databinding;

namespace DatabindingEditor
{
    public static class ReflectionUtils
    {
        public static MemberInfo[] GetFieldsAndProperties(Type type)
        {
            var flags = BindingFlags.Public | BindingFlags.Instance;

            var properties = new List<MemberInfo>();

            if (type.IsInterface)
            {
                var tracked = new HashSet<Type>();
                var queue = new Queue<Type>();

                tracked.Add(type);
                queue.Enqueue(type);

                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (tracked.Contains(subInterface))
                            continue;

                        tracked.Add(subInterface);
                        queue.Enqueue(subInterface);
                    }

                    var typeProperties = subType.GetProperties(flags)
                        .Where(o => !o.HasAttribute<HideInInspector>())
                        .Where(o => !o.Module.Name.Contains("UnityEngine"))
                        .OrderBy(o => o.Name);

                    var props = typeProperties
                        .Where(x => !properties.Contains(x));

                    properties.InsertRange(0, props.Cast<MemberInfo>());
                }

                return properties.ToArray();
            }
            else
            {
                var props = type.GetProperties(flags)
                        .Where(o => !o.HasAttribute<HideInInspector>() && !o.Module.Name.Contains("UnityEngine"))
                        .OrderBy(o => o.Name);

                foreach (var prop in props)
                    if (false == prop.IsSpecialName)
                        properties.Add(prop);

                var fields = type.GetFields(flags)
                        .Where(o => !o.HasAttribute<HideInInspector>() && !o.Module.Name.Contains("UnityEngine"))
                        .OrderBy(o => o.Name);

                foreach (var prop in fields)
                    properties.Add(prop);

                return properties.ToArray();
            }
        }

        public static MethodInfo[] GetMethods(Type type)
        {
            var flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.InvokeMethod;

            var ms = type.GetMethods(flags)
                .Where(o => !o.IsSpecialName)
                .Where(o => !o.HasAttribute<HideInInspector>())
                .Where(o => !o.Module.Name.Contains("UnityEngine"))
                .Where(o => o.GetParameters().Length < 2)
                .Where(o => o.ReturnType == typeof (void) || o.ReturnType == typeof (IEnumerator))
                .OrderBy(o => o.Name)
                .ToArray();

            return ms;
        }
    }
}