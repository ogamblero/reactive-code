using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

using Databinding;

namespace DatabindingEditor
{
    [CustomEditor(typeof (BindingContext))]
    public class BindingContextInspector : Editor
    {
        protected BindingContext _context;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            _context = target as BindingContext;

            var temp = _context.ViewModel;
            _context.ViewModel = (MonoBehaviour)EditorGUILayout.ObjectField("View Model", _context.ViewModel, typeof(MonoBehaviour), true);
            if (temp != _context.ViewModel)
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());

            EditorGUILayout.BeginHorizontal();
            {
                var type = _context.SourceInfo.TryMatchType();
                var typeStr = type != null ? type.FullName : "NULL";

                EditorGUILayout.LabelField("Type", typeStr);
                if (GUILayout.Button("S", GUILayout.Width(24)))
                {
                    var filterTypeName = type != null ? type.FullName : string.Empty;
                    TypeSelector.Show(filterTypeName, t =>
                    {
                        _context.SourceInfo = new BindingContext.TypeInfo(t);
                        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                    });
                }
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}