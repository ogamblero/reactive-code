﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Observable;

using Databinding;

namespace DatabindingEditor
{
    [CustomPropertyDrawer(typeof(Binding))]
    public class BindingDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var binder = property.serializedObject.targetObject as Databinding.Binder;

            if (binder == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            EditorGUI.BeginProperty(position, label, property);
            {
                position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;

                var pathRect = new Rect(position.x, position.y, position.width - 35, position.height);
                var selectRect = new Rect(position.x + position.width - 30, position.y, 25, position.height);

                EditorGUI.PropertyField(pathRect, property.FindPropertyRelative("Path"), GUIContent.none);

                if (GUI.Button(selectRect, "S"))
                {
                    var context = binder.Proxy == null
                    ? binder.GetComponentInParent<BindingContext>()
                    : binder.Proxy.GetComponentInParent<BindingContext>();

                    if (context != null)
                    {
                        Type sourceType = null;
                        var sourceInfo = context.SourceInfo;
                        if (sourceInfo != null)
                            sourceType = sourceInfo.TryMatchType();

                        if (sourceType != null)
                        {
                            var field = binder.GetType().GetField(property.name);
                            var constrains = field.GetAttribute<Databinding.Binder.BindingConstrainsAttribute>();

                            ShowPropertyMenu(sourceType, constrains.FilterTypes, 5, path =>
                                {
                                    var bindingMember = binder.GetType().GetField(property.name);
                                    var binding = bindingMember.GetValue(binder);

                                    var pathMember = binding.GetType().GetField("Path");
                                    pathMember.SetValue(binding, path.Replace('/', '.'));

                                    EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                                });
                        }
                        else
                        {
                            Debug.LogWarning("Can't find binding source.");
                        }
                    }
                    else
                    {
                        Debug.LogWarning("Can't find binding context.");
                    }
                }

                EditorGUI.indentLevel = indent;
            }
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property);
        }

        private static void ShowPropertyMenu(Type baseType, Type[] filterTypes, int depth, Action<string> callback)
        {
            GenericMenu.MenuFunction2 Selected = delegate (object path)
            {
                callback((string)path);
            };

            var props = GetProperties(baseType, filterTypes, "", 0, depth);

            GenericMenu menu = new GenericMenu();

            foreach (string name in props)
                menu.AddItem(new GUIContent(name), false, Selected, name);

            menu.ShowAsContext();
            Event.current.Use();
        }

        private static List<string> GetProperties(Type baseType, Type[] filterTypes, string category, int currentDepth, int depth)
        {
            if (currentDepth > depth)
                return null;

            List<string> result = new List<string>();

            var properties = ReflectionUtils.GetFieldsAndProperties(baseType);
            foreach (MemberInfo mi in properties)
            {
                string name = string.IsNullOrEmpty(category) ? mi.Name : category + "/" + mi.Name;

                if (null != filterTypes && filterTypes.Length > 0)
                {
                    if (filterTypes.Any(f => ValidType(f, mi)))
                        result.Add(name);
                }
                else
                {
                    result.Add(name);
                }
            }

            foreach (MemberInfo mi in properties)
            {
                string name = string.IsNullOrEmpty(category) ? mi.Name : category + "/" + mi.Name;
                var inner = GetProperties(mi.GetParamaterType(), filterTypes, name, currentDepth + 1, depth);

                if (null != inner)
                    result.AddRange(inner);
            }

            return result;
        }

        public static bool ValidType(Type filteredType, MemberInfo i)
        {
            Type paramType = i.GetParamaterType();

            if (typeof(Property).IsAssignableFrom(paramType))
            {
                if (paramType.IsGenericType)
                {
                    Type gType = paramType.GetGenericArguments()[0];
                    return filteredType.IsAssignableFrom(gType);
                }

                PropertyInfo pi = paramType.GetProperty("ValueType").DeclaringType.GetProperty("ValueType");
                Type tType = pi.GetMemberValue<Type>();

                return filteredType.IsAssignableFrom(tType);
            }
            else if (typeof(IReadOnlyProperty).IsAssignableFrom(paramType) && paramType.IsGenericType)
            {
                Type gType = paramType.GetGenericArguments()[0];
                return filteredType.IsAssignableFrom(gType);
            }

            return filteredType.IsAssignableFrom(paramType);
        }
    }
}